import {
    USERS_FETCH_ERROR,
    USERS_FETCH_PENDING,
    USERS_FETCH_SUCCESS,
    USERS_PAGE_CHANGE,
    MODAL_ADD_OPEN,
    MODAL_UPDATE_OPEN,
    MODAL_DELETE_OPEN,
    USERS_UPDATE_BY_ID,
    USERS_DELETE_BY_ID,
    USERS_CREATE_NEW
} from "../constans/page.const"

const PageState = { // khởi tạo giá trị cho state 
    users: [],
    pending: false,
    error: null,
    currentPage: 1,
    limit: 10,
    noPage: 0,
    actionChangeStatus: 0,
    openModelAdd: false,
    openModelEditUser: false,
    openModelDeleteUser: false,

};

const PageReducer = (state = PageState, action) => {
    //console.log(action)
    switch (action.type) {
        case USERS_FETCH_PENDING:
            state.pending = true;
            break;
        case USERS_FETCH_SUCCESS:
            state.pending = false;
            state.noPage = Math.ceil(action.totalUser / state.limit);
            state.users = action.data.slice((state.currentPage - 1) * state.limit, (state.currentPage * state.limit));
            break;
        case USERS_FETCH_ERROR:
            break;
        case USERS_PAGE_CHANGE:
            state.currentPage = action.page;
            break;
        case MODAL_ADD_OPEN:
            state.openModelAdd = action.status;
            break;
        case MODAL_UPDATE_OPEN:
            state.openModelEditUser = action.status;
            break;
        case MODAL_DELETE_OPEN:
            state.openModelDeleteUser = action.status;
            break;
        case USERS_UPDATE_BY_ID:
            state.pending = false;
            state.actionChangeStatus = true;
            break;
        case USERS_DELETE_BY_ID:
            state.pending = false;
            state.actionChangeStatus = true;
            break;
        case USERS_CREATE_NEW:
            state.pending = false;
            state.actionChangeStatus = true;
            break;
        default:
            break;
    }
    // state lưu giá trị mới
    return { ...state }
}
export default PageReducer