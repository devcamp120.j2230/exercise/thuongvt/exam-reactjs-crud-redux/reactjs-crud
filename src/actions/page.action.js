import axios from "axios"; // khai báo axios
import {
    USERS_FETCH_ERROR,
    USERS_FETCH_PENDING,
    USERS_FETCH_SUCCESS,
    USERS_PAGE_CHANGE,
    USERS_UPDATE_BY_ID,
    USERS_DELETE_BY_ID,
    USERS_CREATE_NEW
}
    from "../constans/page.const"

const url = "http://203.171.20.210:8080/devcamp-pizza365/orders";

const axiosCall = async (url, body) => {
    const response = await axios(url, body);
    return response.data;
};

export const fetchUser = (page, limit) => {

    return async (dispatch) => {
        await dispatch({
            type: USERS_FETCH_PENDING
        })

        const responseTotalUser = await fetch(url);

        const dataTotalUser = await responseTotalUser.json();

        const params = new URLSearchParams({
            _start: (page - 1) * limit,
            _limit: limit
        });

        axiosCall(url + "?" + params.toString())
            .then(result => {
                console.log(params.toString())
                return dispatch({
                    type: USERS_FETCH_SUCCESS,
                    totalUser: dataTotalUser.length,
                    data: result
                })
            })
            .catch(
                error => {
                    return dispatch({
                        type: USERS_FETCH_ERROR,
                        error: error
                    })
                }
            )
    }
}

export const upDateUserById = (IdUser, data) => {
    return async (dispatch) => {
        await dispatch({
            type: USERS_FETCH_PENDING
        })

        var config = {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };

        axiosCall(url + "/" + IdUser, config)
            .then(result => {
                console.log(result);
                return dispatch({
                    type: USERS_UPDATE_BY_ID,
                })
            })
            .catch(error => {
                return dispatch({
                    type: USERS_FETCH_ERROR,
                    error
                })
            });
    }
}


export const deleteUserById = (IdUser) => {
    return async (dispatch) => {
        await dispatch({
            type: USERS_FETCH_PENDING
        })
        var config = {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
        };
        axiosCall(url + "/" + IdUser, config)
            .then(result => {
                console.log(result);
                return dispatch({
                    type: USERS_DELETE_BY_ID,
                })
            })
            .catch(error => {
                return dispatch({
                    type: USERS_FETCH_ERROR,
                    error
                })
            });
    }
}

export const createNewUser = (data) => {
    return async (dispatch) => {
        await dispatch({
            type: USERS_FETCH_PENDING
        })
        var config = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            data: data
        };
        axiosCall(url, config)
            .then(result => {
                console.log(result)
                return dispatch({
                    type: USERS_CREATE_NEW,
                })
            })
            .catch(error => {
                return dispatch({
                    type: USERS_FETCH_ERROR,
                    error
                })
            });

    }
}

export const pageChangePagination = (page) => {
    return {
        type: USERS_PAGE_CHANGE,
        page: page
    }
}

