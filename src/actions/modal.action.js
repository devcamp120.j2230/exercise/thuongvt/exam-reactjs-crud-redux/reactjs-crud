import {
    MODAL_ADD_OPEN,
    MODAL_DELETE_OPEN,
    MODAL_UPDATE_OPEN,
} from "../constans/page.const"

// open modal add user 
export const modalAddOpenAction = () => {
    return {
        type: MODAL_ADD_OPEN,
        status: true
    }
};
//close madal add user
export const modalAddCloseAction = ()=>{
    return {
        type: MODAL_ADD_OPEN,
        status: false
    }
}

// open modal edit user 
export const modalEditOpenAction = () => {
    return {
        type: MODAL_UPDATE_OPEN,
        status: true
    }
};
//close modal edit user
export const modalEditCloseAction = ()=>{
    return {
        type: MODAL_UPDATE_OPEN,
        status: false
    }
}
//open Modal delete user
export const modalDeleteOpenAction =()=>{
    return {
        type: MODAL_DELETE_OPEN,
        status: true
    }
}

//open Modal delete user
export const modalDeleteCloseAction =()=>{
    return {
        type: MODAL_DELETE_OPEN,
        status: false
    }
}