// const xử lý sựu kiện load trang
export const USERS_FETCH_PENDING = "USERS_FETCH_PENDING";
export const USERS_FETCH_SUCCESS = "USERS_FETCH_SUCCESS";
export const USERS_FETCH_ERROR = "USERS_FETCH_ERROR";
export const USERS_PAGE_CHANGE = "USERS_PAGE_CHANGE";
// xử lý sự kiện đóng mở các modal
export const MODAL_ADD_OPEN = "Mở modal add user";
export const MODAL_UPDATE_OPEN = "Mở modal update user";
export const MODAL_DELETE_OPEN = "Mở modal delete user ";

// xử lý sự kiện thêm sửa xóa user
export const USERS_UPDATE_BY_ID = "cập nhật user thông qua id"
export const USERS_DELETE_BY_ID = "cập nhật user thông qua id"
export const USERS_CREATE_NEW = "tạo mới dữ liệu thành công"