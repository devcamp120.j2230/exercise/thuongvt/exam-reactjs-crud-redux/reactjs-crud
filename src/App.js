import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import DataUser from './components/data.user';

function App() {
  return (
    <div>
      <DataUser></DataUser>
    </div>
  );
}

export default App;
