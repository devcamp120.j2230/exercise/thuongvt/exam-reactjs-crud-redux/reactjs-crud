import { Alert, Grid, Modal, Snackbar } from "@mui/material";
import Box from '@mui/material/Box';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Container, Row } from "reactstrap";
import { modalEditCloseAction } from "../actions/modal.action";
import { upDateUserById } from "../actions/page.action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 700,
    bgcolor: "green",
    boxShadow: 24,
    p: 4,
};

function ModalEditUser ({dataEdit}){


    const getData = async (paramUrl, paramOption = {}) => {
        const response = await fetch(paramUrl, paramOption);
        const data = await response.json();
        return data
    }
    
    //Khai báo một số tate sử dụng nội bộ funcion
    const [drinks,setDrink] = useState([])
    const [openAlert, setOpenAlert] = useState(false); // đóng mở alert
    const [alertForm, setAlertForm] = useState(""); // hiển thị thông báo alert
    const [trangThai,setTrangThai] = useState("")

   
   const dispatch = useDispatch()
     
   const {openModelEditUser} = useSelector((reduxData)=>{
    return reduxData.PageReducer
   })

    // sự kiện khi ấn nút hủy bỏ thêm user
    const clickBtnHuyBo = () => {
        dispatch(modalEditCloseAction())
        setOpenAlert(true)
        setAlertForm("Hủy bỏ sửa user")
    }
    // Sự kiện khi đóng alert
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }

    const clickBtnSua =()=>{
        let abc = {
            trangThai:trangThai
        }
        dispatch(upDateUserById(dataEdit.id,abc))
        setOpenAlert(true)
        setAlertForm("sửa trạng thái user thành công")
        clickBtnHuyBo()

    }

    useEffect(() => {
        setTrangThai(dataEdit.trangThai)
        //Lấy danh sách đồ uống
        getData("http://203.171.20.210:8080/devcamp-pizza365/drinks")
        .then((data)=>{
           //console.log(data)
            setDrink(data)
        })
        .catch((err) => {
            console.error(err.message)
        })
    }, [openModelEditUser])



    return(
        <Grid>
            <Modal
                open={openModelEditUser}
                onClose={clickBtnHuyBo}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Container style={{ maxHeight: "600px" }}>
                        <Row>
                            <form>
                                <div className="row border-secondary">
                                    <div className="col-sm-12 border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Kích cỡ</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <select name=""  class="form-control" value={dataEdit.kichCo}>
                                                    <option selected>Chọn Combo Pizza</option>
                                                    <option value="S">Size S</option>
                                                    <option value="M">Size M</option>
                                                    <option value="L">Size L</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Đường kính</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={dataEdit.duongKinh} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Sườn nướng</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={dataEdit.suon}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12  border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Salad</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={dataEdit.salad}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Số lượng nước</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={dataEdit.soLuongNuoc} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12  border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Thành Tiền(VND)</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={dataEdit.thanhTien} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12  border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Loại pizza</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <select name="" className="form-control" value={dataEdit.loaiPizza} >
                                                    <option value="all">Tất cả các loại Pizza</option>
                                                    <option value="Seafood">Hải sản</option>
                                                    <option value="Hawaii">Hawaii</option>
                                                    <option value="Bacon">Thịt hun khói</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group  border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Đồ uống</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <select name="" className="form-control" value={dataEdit.idLoaiNuocUong}>
                                                    {drinks.map((e,i)=>{
                                                        return <option key={i} value={e.maNuocUong}>{e.tenNuocUong}</option>
                                                    })}  
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group  border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Trạng Thai</label>
                                            </div>
                                            <div className="col-sm-8">
                                            <select name="" className="form-control" value={trangThai} onChange={(e)=>setTrangThai(e.target.value)}>
                                                    {/* <option value="">Trạng thái đơn hàng</option> */}
                                                    <option value="open">Open</option>
                                                    <option value="cancel">Hủy</option>
                                                    <option value="confirmed">Xác nhận</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Họ và tên</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={dataEdit.hoTen} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                            <label for="">Email</label>
                                            </div>
                                            <div className="col-sm-8">
                                            <input type="text" className="form-control"  placeholder="...."value={dataEdit.email} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                            <label for="">Số điện thoại</label>
                                            </div>
                                            <div className="col-sm-8">
                                            <input type="text" className="form-control"  placeholder="...." value={dataEdit.soDienThoai} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                            <label for="">Địa chỉ</label>
                                            </div>
                                            <div className="col-sm-8">
                                            <input type="text" className="form-control"  placeholder="...." value={dataEdit.diaChi} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                            <label for="">Lời nhắn</label>
                                            </div>
                                            <div className="col-sm-8">
                                            <input type="text" className="form-control"  placeholder="...." value={dataEdit.loiNhan} />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </Row>
                        <Row style={{textAlign: "center",marginTop:"5px"}}>
                            <Col><Button style={{ width: "100px" }} onClick={clickBtnSua}>Sửa</Button></Col>
                            <Col><Button style={{ width: "100px" }} onClick={clickBtnHuyBo}>Hủy Bỏ</Button></Col>
                        </Row>
                    </Container>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert sx={{ width: '100%' }}>
                    {alertForm}
                </Alert>
            </Snackbar>
        </Grid>
    )
}
export default ModalEditUser