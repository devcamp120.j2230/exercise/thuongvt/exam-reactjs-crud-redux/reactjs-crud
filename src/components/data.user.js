import { CircularProgress, Container, Grid, Pagination, Typography } from "@mui/material"
import { useEffect, useState } from "react"
import { Table } from "react-bootstrap"
import { useDispatch, useSelector } from "react-redux"
import { Button } from "reactstrap"
import { modalAddOpenAction, modalDeleteOpenAction, modalEditOpenAction } from "../actions/modal.action"
import { fetchUser, pageChangePagination } from "../actions/page.action"
import ModalDeleteUser from "./modal.delete.user"
import ModelAddNewUser from "./model.add.user"
import ModalEditUser from "./model.edit.user"



function DataUser() {

    const [dataUserEdit, setDataUserEdit] = useState([])
    const [dataUserDelete, setDataUserDelete] = useState([])

    // bắn sự kiện đi tới store
    const dispatch = useDispatch()

    // Lấy state từ Page reducer
    const { users, currentPage, limit, noPage, pending, actionChangeStatus } = useSelector((reduxData) => {
        return (
            reduxData.PageReducer
        )
    })

    // lấy dữ liệu khi mỗi lần render lại
    useEffect(() => {
        //Gọi API lấy dữ liệu
        dispatch(fetchUser(currentPage, limit))
    }, [currentPage, actionChangeStatus])


    // Sự kiện ChangePagination
    const onChangePagination = (event, value) => {
        dispatch(pageChangePagination(value));
    }

    // sự kiện ấn nút sửa user
    const clickBtnSua = (elment) => {
        dispatch(modalEditOpenAction())
        setDataUserEdit(elment)
    }



    //Sự kiện khi ấn nút xóa
    const clickBtnXoa = (elment) => {
        dispatch(modalDeleteOpenAction())
        setDataUserDelete(elment)
    }



    // sự kiện khi ấn nút thêm người dùng mới
    const clickBtnAddUser = () => {
        dispatch(modalAddOpenAction())
    }



    return (
        <Container>
            <Grid container mt={5}>
                {
                    pending ?
                        <Grid item lg={12} md={12} sm={12} xs={12} textAlign="center">
                            <CircularProgress />
                        </Grid>
                        : <>
                            <Grid item xs={12} sm={12} md={12} lg={12}>
                                <Typography style={{ textAlign: "center", backgroundColor: "green", color: "white", height: "50px" }}>
                                    <h3>Danh sách người dùng</h3>
                                </Typography>
                            </Grid>
                            <Grid item xs={12} sm={12} md={12} lg={12} >
                                <Button color="primary" style={{ margin: "10px" }} onClick={clickBtnAddUser} >Thêm khách hàng mới</Button>
                            </Grid>
                            <Grid item xs={12} sm={12} md={12} lg={12}>
                                <Table striped bordered hover style={{ margin: "auto", textAlign: "center" }}>
                                    <thead>
                                        <tr>
                                            <th>OrDer ID</th>
                                            <th>Họ và Tên</th>
                                            <th>Số điện thoại</th>
                                            <th>Kích cỡ combo</th>
                                            <th>Loại Pizza</th>
                                            <th>Nước Uống</th>
                                            <th>Thành Tiền</th>
                                            <th>Trạng thái</th>
                                            <th>Chi tiết</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {
                                            users.map((elment, index) => {
                                                return (
                                                    <tr key={index}>
                                                        <th>{elment.orderCode}</th>
                                                        <th>{elment.hoTen}</th>
                                                        <th>{elment.soDienThoai}</th>
                                                        <th>{elment.kichCo}</th>
                                                        <th>{elment.loaiPizza}</th>
                                                        <th>{elment.idLoaiNuocUong}</th>
                                                        <th>{elment.thanhTien}</th>
                                                        <th>{elment.trangThai}</th>
                                                        <th><Button color="success" style={{ margin: "2px" }} onClick={() => { clickBtnSua(elment) }}>Sửa</Button><Button color="danger" style={{ margin: "2px" }} onClick={() => { clickBtnXoa(elment) }}>Xóa</Button></th>
                                                    </tr>
                                                )
                                            })
                                        }
                                    </tbody>
                                </Table>
                            </Grid>
                            <Grid item lg={12} md={12} sm={12} xs={12} mt={5}>
                                <Pagination count={noPage} page={currentPage} onChange={onChangePagination} />
                            </Grid>
                            <>
                                <ModelAddNewUser
                                >
                                </ModelAddNewUser>
                            </>
                            <>
                                <ModalEditUser
                                    dataEdit={dataUserEdit}
                                >
                                </ModalEditUser>
                            </>
                            <>
                                <ModalDeleteUser
                                    dataDelete={dataUserDelete}
                                >

                                </ModalDeleteUser>
                            </>
                        </>
                }
            </Grid>
        </Container>
    )
}

export default DataUser