import { Alert, Grid, Modal, Snackbar } from "@mui/material";
import Box from '@mui/material/Box';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Container, Row } from "reactstrap";
import { modalDeleteCloseAction } from "../actions/modal.action";
import { deleteUserById } from "../actions/page.action";


const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 400,
    bgcolor: "blue",
    boxShadow: 24,
    p: 4,
};


function ModalDeleteUser({ dataDelete }) {

    //Khai báo một số state sử dụng nội bộ funcion
    const [openAlert, setOpenAlert] = useState(false);
    const [alertForm, setAlertForm] = useState("");
    const [id, setId] = useState("")


    const dispatch = useDispatch()

    const { openModelDeleteUser } = useSelector((reduxData) => {
        return reduxData.PageReducer
    })

    // Hàm click hủy bỏ xóa 
    const clickBtnHuyBo = () => {
        dispatch(modalDeleteCloseAction())
        setOpenAlert(true)
        setAlertForm("Hủy bỏ xóa user")
    }
    // hàm xóa bỏ alert
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    // Hàm xóa bỏ người dùng
    const clickBtnXoa = () => {
        dispatch(deleteUserById(id))
        setOpenAlert(true)
        setAlertForm("xóa user")
        clickBtnHuyBo()
    }

    // cập nhật Id mỗi lần render lại
    useEffect(() => {
        setId(dataDelete.id)
    }, [openModelDeleteUser])


    return (
        <Grid>
            <Modal
                open={openModelDeleteUser}
                onClose={clickBtnHuyBo}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Col style={{ textAlign: "center", color: "white" }}><h3>Xóa người dùng</h3></Col>
                    <Container>
                        <Row>
                            <div style={{ textAlign: "center", color: "white" }}>
                                <p>Bạn có muốn xóa thông tin người dùng</p>
                            </div>
                        </Row>
                        <Row style={{ marginTop: "10px", textAlign: "center" }}>
                            <Col><Button style={{ width: "100px", backgroundColor: "yellowgreen" }} onClick={clickBtnXoa} > Xóa User</Button></Col>
                            <Col><Button style={{ width: "100px", backgroundColor: "orange" }} onClick={clickBtnHuyBo} >Hủy Bỏ</Button></Col>
                        </Row>
                    </Container>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert sx={{ width: '100%' }}>
                    {alertForm}
                </Alert>
            </Snackbar>
        </Grid>
    )
}

export default ModalDeleteUser