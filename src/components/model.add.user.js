import { Alert, Grid, Modal, Snackbar } from "@mui/material";
import Box from '@mui/material/Box';
import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button, Col, Container, Row } from "reactstrap";
import { modalAddCloseAction } from "../actions/modal.action";
import { createNewUser } from "../actions/page.action";

const style = {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    width: 600,
    bgcolor: "orange",
    boxShadow: 24,
    p: 4,
};

function ModelAddNewUser() {


    const getData = async (paramUrl, paramOption = {}) => {
        const response = await fetch(paramUrl, paramOption);
        const data = await response.json();
        return data
    }


    // Khai báo một số state dùng nội bộ chương trình
    const [openAlert, setOpenAlert] = useState(false);
    const [openAlertWarning, setOpenAlertWarning] =useState(false)
    const [alertForm, setAlertForm] = useState(""); 
    const [validate, setValidate] = useState(false);
    const [giam, setGiam] = useState()
    
    const [drinks, setDrink] = useState([])
    const [kichCo, setKichCo] = useState("");
    const [duongKinh, setDuongKinh] = useState();
    const [suon, setSuon] = useState();
    const [salad, setSalad] = useState();
    const [soLuongNuoc, setSoLuongNuoc] = useState();
    const [thanhTien, setThanhTien] = useState();
    const [giamGia, setGiamGia] =useState()
    const [idVourcher, setIdVourcher] = useState("");
    const [hoTen, setHoTen] = useState("");
    const [email, setEmail] = useState("");
    const [soDienThoai, setSoDienThoai] = useState("");
    const [diaChi, setDiaChi] = useState("");
    const [loiNhan, setLoiNhan] = useState("");

    const [idLoaiNuocUong, setIdLoaiNuocUong] = useState("");
    const [loaiPizza, setLoaiPizza] = useState("Seafood");

    const dispatch = useDispatch();

    const {openModelAdd} = useSelector((reduxData)=>{
        return reduxData.PageReducer
    })


    // Hàm chọn comboPizza
    const changePizzaSize = (e)=>{
        setKichCo(e.target.value);
        switch(e.target.value){
            case "S":
                setDuongKinh(20);
                setSuon(2);
                setSalad(200);
                setSoLuongNuoc(2);
                setThanhTien(150000);
                break;
            case "M":
                setDuongKinh(25);
                setSuon(4);
                setSalad(300);
                setSoLuongNuoc(3);
                setThanhTien(200000);
                break;
            case "L":
                setDuongKinh(30);
                setSuon(8);
                setSalad(500);
                setSoLuongNuoc(4);
                setThanhTien(250000);
                break;
            default:
                break;
        }
    }

    // Sự kiện click hủy bỏ add user
    const clickBtnHuyBo = ()=>{ 
        dispatch(modalAddCloseAction())
        setOpenAlert(true)
        setAlertForm("hủy bỏ thêm user")
    }
    // Sự kiện khi đóng alert
    const handleCloseAlert = () => {
        setOpenAlert(false)
    }
    // sự kiện đóng alerwarning
    const handleCloseAlertWarning = ()=>{
        setOpenAlertWarning(false)
    }
    // Sự kiện khi ấn nút clickBtn add
    const clickBtnAdd = ()=>{

        // dữ liệu
        var data = {
            kichCo,
            duongKinh,
            suon,
            salad,
            loaiPizza,
            idVourcher,
            idLoaiNuocUong,
            soLuongNuoc,
            hoTen,
            thanhTien,
            email,
            soDienThoai,
            diaChi,
            loiNhan,
            giamGia
        }
        if(validateData(data)){
            setValidate(true)
            dispatch(createNewUser(data))
            setOpenAlert(true)
            setAlertForm("tạo mới thành công")
            clickBtnHuyBo()
        }
        else{
            setValidate(false)
        }

    }

    // Hàm kiểm tra data
    const validateData = (data)=>{

        if (data.kichCo === "") {
            setOpenAlertWarning(true)
            setAlertForm("Không để trống kích cỡ")
            return false;
        };
        if (data.loaiPizza === "all") {
            setOpenAlertWarning(true)
            setAlertForm("Không để trống loại pizza")
            return false;
        };
        if (data.idLoaiNuocUong === "") {
            setOpenAlertWarning(true)
            setAlertForm("Không để trống nước uống")
            return false;
        };
        // hàm kiểm tra voucher
        checkVoucherId (data.idVourcher);

        if (data.hoTen === "") {
            setOpenAlertWarning(true)
            setAlertForm("Không để trống họ tên")
            return false;
        };
        if (data.soDienThoai === "") {
            setOpenAlertWarning(true)
            setAlertForm("Không để trống số điện thoại")
            return false;
        };
        if (data.email=== "") {
            setOpenAlertWarning(true)
            setAlertForm("Không để trống email")
            return false;
        };
        if (data.email=== "") {
            setOpenAlertWarning(true)
            setAlertForm("Không để trống email")
            return false;
        };
        if (data.diaChi=== "") {
            setOpenAlertWarning(true)
            setAlertForm("Không để trống địa chỉ")
            return false;
        };
        return true
    }



    const checkVoucherId = (param)=>{
        if(param!==""){
            getData("http://203.171.20.210:8080/devcamp-pizza365/vouchers/" + param)
            .then((data)=>{
                console.log(data.discount)
                if(data.discount!=-1){
                    setGiam(data.discount)
                    setGiamGia(thanhTien*(1-giam/100))
                    setValidate(false)

                }
                else{
                    setValidate(true)
                    setGiam(0)
                    setGiamGia(thanhTien*(1-giam/100))
                }
            })
            .catch((err)=>{
                console.error(err.maseage)
                setValidate(true)
            })
        }
        setGiam(0)
        setGiamGia(thanhTien*(1-giam/100))
        return setValidate(true)
        
    }


    useEffect(() => {
        //Lấy danh sách đồ uống
        getData("http://203.171.20.210:8080/devcamp-pizza365/drinks")
        .then((data)=>{
            setDrink(data)
        })
        .catch((err) => {
            console.error(err.message)
        })
    }, [openModelAdd])

    
    return (
        <Grid>
            <Modal
                open={openModelAdd}
                onClose={clickBtnHuyBo}
                aria-labelledby="modal-modal-title"
                aria-describedby="modal-modal-description"
            >
                <Box sx={style}>
                    <Container style={{ maxHeight: "600px" }}>
                        <Row>
                            <form>
                                <div className="row border-secondary">
                                    <div className="col-sm-12 border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Kích cỡ</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <select name=""  class="form-control" value={kichCo} onChange={changePizzaSize}>
                                                    <option value="" selected>Chọn Combo Pizza</option>
                                                    <option value="S">Size S</option>
                                                    <option value="M">Size M</option>
                                                    <option value="L">Size L</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Đường kính</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={duongKinh}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Sườn nướng</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={suon} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12  border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Salad</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={salad} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Số lượng nước</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={soLuongNuoc} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12  border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Đơn giá (VND)</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={thanhTien} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12  border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Loại pizza</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <select name="" className="form-control" value={loaiPizza} onChange={(e)=>setLoaiPizza(e.target.value)}>
                                                    <option  value="all" selected >Tất cả các loại Pizza</option>
                                                    <option value="Seafood">Hải sản</option>
                                                    <option value="Hawaii" >Hawaii</option>
                                                    <option value="Bacon">Thịt hun khói</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group  border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Đồ uống</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <select name="" className="form-control" value={idLoaiNuocUong} onChange={(e)=>setIdLoaiNuocUong(e.target.value)}>
                                                <option selected value="">Chọn đồ uống</option>
                                                {drinks.map((e,i)=>{
                                                        return <option key={i} value={e.maNuocUong}>{e.tenNuocUong}</option>
                                                    })}  
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group  border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Voucher ID</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={idVourcher} onChange={(e)=>setIdVourcher(e.target.value)}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                                <label for="">Họ và tên</label>
                                            </div>
                                            <div className="col-sm-8">
                                                <input type="text" className="form-control" placeholder="...." value={hoTen} onChange={(e)=>setHoTen(e.target.value)} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                            <label for="">Email</label>
                                            </div>
                                            <div className="col-sm-8">
                                            <input type="text" className="form-control"  placeholder="...." value={email} onChange={(e)=>setEmail(e.target.value)} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                            <label for="">Số điện thoại</label>
                                            </div>
                                            <div className="col-sm-8">
                                            <input type="text" className="form-control"  placeholder="...." value={soDienThoai} onChange={(e)=>setSoDienThoai(e.target.value)} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                            <label for="">Địa chỉ</label>
                                            </div>
                                            <div className="col-sm-8">
                                            <input type="text" className="form-control"  placeholder="...."  value={diaChi} onChange={(e)=>setDiaChi(e.target.value)}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                            <label for="">Lời nhắn</label>
                                            </div>
                                            <div className="col-sm-8">
                                            <input type="text" className="form-control"  placeholder="...." value={loiNhan} onChange={(e)=>setLoiNhan(e.target.value)}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12 form-group border-info">
                                        <div className="row">
                                            <div className="col-sm-4">
                                            <label for="">Giảm giá</label>
                                            </div>
                                            <div className="col-sm-8">
                                            <input type="text" className="form-control"  placeholder="...." value={giamGia}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </Row>
                        <Row style={{textAlign: "center",marginTop:"5px"}}>
                            <Col><Button style={{ width: "100px" }} onClick={clickBtnAdd}>Add</Button></Col>
                            <Col><Button style={{ width: "100px" }} onClick={clickBtnHuyBo}>Hủy Bỏ</Button></Col>
                        </Row>
                    </Container>
                </Box>
            </Modal>
            <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
                <Alert severity="success" sx={{ width: '100%', color:"blue" }}>
                    {alertForm}
                </Alert>
            </Snackbar>
            <Snackbar open={openAlertWarning} autoHideDuration={6000} onClose={handleCloseAlertWarning}>
                <Alert severity="warning" sx={{ width: '100%', color:"red" }}>
                    {alertForm}
                </Alert>
            </Snackbar>
        </Grid>
    )
}
export default ModelAddNewUser